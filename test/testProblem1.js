const getBoard = require("../problem1")
const boardId = '663069065529b650321d2cc3';
getBoard(boardId)
    .then(boardData => {
        console.log('Board Data:', boardData);
    })
    .catch(error => {
        console.error('Error:', error.message);
    });

