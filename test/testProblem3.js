const getLists = require("../problem3");
const boardId = '663327fecf9cad304f3b8bee';
getLists(boardId)
  .then((lists) => {
    console.log("get lists:", lists);
  })
  .catch((error) => {
    console.error("Error:", error.message);
  });
