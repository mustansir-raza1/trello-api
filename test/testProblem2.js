const createBoard = require("../problem2");
const boardName = "MustansirRaza";
createBoard(boardName)
  .then((boardData) => {
    console.log("Board Data:", boardData);
  })
  .catch((error) => {
    console.error("Error:", error.message);
  });
