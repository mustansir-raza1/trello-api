const {getAllLists, deleteList, deleteAllLists } = require("../problem8.js");
//const getAllLists = require("../problem8")
const boardId = '66333ebc3438694b8c245c2c';
const listId = '66333eefb19e76d908a93a2f';
getAllLists(boardId)
.then((lists) => {
    console.log("get lists:", lists);
  })
  .catch((error) => {
    console.error("Error:", error.message);
  });
deleteList(listId)
.then(()=>{
  console.log("deleteList:")
})
.catch((error)=>{
  console.error(error)
})

deleteAllLists(boardId)
.then(()=>{
    console.log('All lists deleted successfully!')
})
.catch(error=>{
    console.error(error)
})