const {createBoard, createList, createCard} = require("../problem6");
createBoard("SoftWare Developer")

const lists = ['HTML', 'CSS', 'JavaScript'];
const cardNames = ['Card 1', 'Card 2', 'Card 3'];

Promise.all(
    lists.map((listId, index) => {
        return createList(listId)
            .then(list => createCard(list.id, cardNames[index]));
    })
).then(() => {
    console.log('Lists and cards created successfully!');
}).catch(error => {
    console.error('Error:', error);
});