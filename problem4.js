const apiKey = "81c0065ad9b93e46b28d0428261d82ee";
const apiToken =
  "ATTA69cf442588b352a4f4711a29c5850845f4609e96d9371beb6e0a1db9251b18918F8F5D44";

// Function to fetch board data from Trello API
function getCards(listId) {
  const url = `https://api.trello.com/1/lists/${listId}/cards?key=${apiKey}&token=${apiToken}`;
  return fetch(url, {
    method: "GET",
    headers: {
      Accept: "application/json",
    },
  }).then((response) => {
    if (response) {
      return response.json();
    } else {
      throw new Error("Failed to fetch board data");
    }
  });
}
module.exports = getCards;
