const getCards = require("./problem4.js");
const getLists = require("./problem3.js");

function getAllCards(boardId) {
  return new Promise((resolve, reject) => {
    getLists(boardId).then((lists) => {
      const getListsId = lists.map((list) => list.id);
      const getCardsId = getListsId.map((list) => getCards(list));
      return Promise.all(getCardsId);
    })
    .then((data)=>{
      resolve(data);
    })
    .catch((error)=>{
      reject(error)
    })
  })
}

module.exports = getAllCards;
