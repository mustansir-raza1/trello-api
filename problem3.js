const apiKey = "81c0065ad9b93e46b28d0428261d82ee";
const apiToken =
  "ATTA69cf442588b352a4f4711a29c5850845f4609e96d9371beb6e0a1db9251b18918F8F5D44";

  
// Create a function createBoard which takes the boardName as argument and returns a promise which resolves with newly created board data
function getLists(boardId) {
  const url = `https://api.trello.com/1/boards/${boardId}/lists?key=${apiKey}&token=${apiToken}`;
  return fetch(url, {
    method: "GET",
    headers: {
      Accept: "application/json",
    },
  }).then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      console.log("getting error");
    }
  });
}
module.exports = getLists;
