const apiKey = "81c0065ad9b93e46b28d0428261d82ee";
const apiToken = "ATTA69cf442588b352a4f4711a29c5850845f4609e96d9371beb6e0a1db9251b18918F8F5D44";

function UncheckItemsAll(itemsId,cardId){
    return fetch(`https://api.trello.com/1/cards/${cardId}/checkItem/${itemsId}?state=incomplete&key=${apiKey}&token=${token}`, {
        method: 'PUT'
        })
        .then(response => {
            return response.json();
        })
}
function getAllCheckList(boardID) {
    return fetch(
      `https://api.trello.com/1/boards/${boardID}/checklists?key=${apiKey}&token=${apiToken}`,
      {
        method: "GET",
      }
    ).then((response) => {
      return response.json();
    });
  }

  function getCheckItemsData(checkListId) {
    return fetch(
      `https://api.trello.com/1/checklists/${checkListId[0]}/checkItems?key=${apiKey}&token=${apiToken}`,
      {
        method: "GET",
      }
    )
      .then((response) => {
        return response.json();
      })
      .then((checkitemdata) => {
          const delayedCheck = (data, index) => {
              return new Promise((resolve) => {
                if (index === 0) {
                  resolve(UncheckItemsAll(data.id, checkListId[1]));
                } else {
                  setTimeout(() => {
                    resolve(UncheckItemsAll(data.id, checkListId[1]));
                  }, 4000 * index); 
                }
              });
          };
          return Promise.all(checkitemdata.map((data, index) => delayedCheck(data, index)));
      })
  }
  
  function updateAllCheckitmesSequentially(boardID) {
  
      return  getAllCheckList(boardID)
          .then((checkList) => {
              let checkListId = checkList.reduce((prevData, currData) => {
                  let newCurrData = [];
                  newCurrData.push(currData.id);
                  newCurrData.push(currData.idCard);
                  prevData.push(newCurrData);
                  return prevData;
              }, []);
              return checkListId;
          })
          .then((checkListId) => {
              return Promise.all(checkListId.map((id) => getCheckItemsData(id)));
          })
          .then(() => "All checkitems in checklist is incompeleted sequentially")
  }
  
  
  module.exports = updateAllCheckitmesSequentially
  
  