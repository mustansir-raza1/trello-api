const apiKey = "81c0065ad9b93e46b28d0428261d82ee";
const apiToken =
  "ATTA69cf442588b352a4f4711a29c5850845f4609e96d9371beb6e0a1db9251b18918F8F5D44";
const boardId = '66333ebc3438694b8c245c2c';

function createBoard(boardName) {
    const url = `https://api.trello.com/1/boards?key=${apiKey}&token=${apiToken}&name=${encodeURIComponent(
  boardName
)}`;
  return fetch(url, {
    method: "POST",
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error(`Failed to create board: ${response.statusText}`);
      }
      return response.json();
    })
    .then((board) => {
      console.log("Board created successfully:");
      console.log(board);
    })
    .catch((error) => {
      console.error("Error creating board:", error);
    });
}

function createList(listName) {
    return fetch(`https://api.trello.com/1/lists?key=${apiKey}&token=${apiToken}&name=${listName}&idBoard=${boardId}`, {
        method: 'POST'
    }).then(response => response.json());
}

function createCard(listId, cardName) {
    return fetch(`https://api.trello.com/1/cards?key=${apiKey}&token=${apiToken}&name=${cardName}&idList=${listId}`, {
        method: 'POST'
    }).then(response => response.json());
}

module.exports = { createBoard, createList, createCard };
